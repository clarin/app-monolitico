package com.javacodegeeks.example.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javacodegeeks.example.config.ControllerAdvice;
import com.javacodegeeks.example.domain.Product;
import com.javacodegeeks.example.repository.ProductRepository;
import com.javacodegeeks.example.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@Slf4j
public class ProductControllerTest {

    private static MockMvc mockMvc;

    @Before
    public void init(){
        ProductRepository productRepository= new ProductRepository();
        ProductService productService= new ProductService(productRepository);
        ProductController productController= new ProductController(productService);

        mockMvc = MockMvcBuilders.standaloneSetup(productController)
                .setControllerAdvice(new ControllerAdvice())
                .build();
    }


    @Test
    public void viewHomePage() throws Exception {

        MvcResult mvcResult= mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        final String jsonMessage= mvcResult.getResponse().getContentAsString();

        log.info("jsonMessage {},",jsonMessage);
    }

    @Test
    public void showNewProductPage() throws Exception {

        MvcResult mvcResult= mockMvc.perform(MockMvcRequestBuilders.get("/new"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        final String jsonMessage= mvcResult.getResponse().getContentAsString();

        log.info("jsonMessage {},",jsonMessage);
    }

    @Test
    public void saveProduct() throws Exception {

        Product  product= new Product(7L,"Leche","Gloria","Peru",1.50f);
        ObjectMapper objectMapper= new ObjectMapper();
        String jsonBody= objectMapper.writeValueAsString(product);

        MvcResult mvcResult= mockMvc.perform(MockMvcRequestBuilders.post("/save")
                .flashAttr("product",product))
                .andExpect(MockMvcResultMatchers.status().isFound()).andReturn();

        final String jsonMessage= mvcResult.getResponse().getContentAsString();

        log.info("jsonMessage {},",jsonMessage);
    }
}