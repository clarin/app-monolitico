package com.javacodegeeks.example.repository;

import com.javacodegeeks.example.domain.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepository {

    private static List<Product> list= new ArrayList<>();

    static {
        list.add(new Product(1L,"jabon","patito","Peru",1.60f));
        list.add(new Product(2L,"alcohol","marina","Peru",5.00f));
        list.add(new Product(3L,"mascarilla","ninet","china",4.20f));
        list.add(new Product(4L,"guantes","alicorp","China",8.50f));
    }

    public List<Product> getAllProducts(){
        return list;
    }

    public void save(Product product){
        list.add(product);
    }
}
