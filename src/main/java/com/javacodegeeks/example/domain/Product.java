package com.javacodegeeks.example.domain;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {
	private Long id;
	private String name;
	private String brand;
	private String madein;
	private float price;

	public Product() {
	}

	public Product(Long id, String name, String brand, String madein, float price) {
		super();
		this.id = id;
		this.name = name;
		this.brand = brand;
		this.madein = madein;
		this.price = price;
	}


}
