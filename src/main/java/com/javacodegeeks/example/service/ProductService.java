package com.javacodegeeks.example.service;

import com.javacodegeeks.example.domain.Product;
import com.javacodegeeks.example.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

	private final ProductRepository repo;

	public ProductService(ProductRepository repo) {
		this.repo = repo;
	}

	public List<Product> listAll() {
		return repo.getAllProducts();
	}
	
	public void save(Product product) {
		repo.save(product);
	}

}
